export const setMenuItems = (state, data) => {
    // 获取菜单数据
    state.MenuItems = data
}
export const removeMenuItems = (state, data) => {
    // 管理菜单   删除品种
    state.MenuItems.splice((state.MenuItems.indexOf()), 1)
}
export const pushToMenuItems = (state, data) => {
    //管理 添加品种
    state.MenuItems.push(data)
}
export const userStatus = (state, user) => {
    // 登录 actions应用这个方法
    if (user) {
        state.currentUser = user[0].email;
        state.isLogin = true;
        state.userName = user[0].userName;
    } else {
        state.currentUser = '';
        state.isLogin = false
        state.userName = '';
    }

}
export const stateIns = (state,ins) => {
    state.ins = ins
}
export const isLoading = (state, login) => {
    state.isLogin = login
}
