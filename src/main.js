// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// Element
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);


import 'bootstrap/dist/css/bootstrap.min.css'
// import 'bootstrap/dist/js/bootstrap.min.js'
// iview
// import iView from 'iview';
// import 'iview/dist/styles/iview.css';    // 使用 CSS
// Vue.use(iView);
// vuex

import { store } from './store'


// axios
import axios from 'axios'
Vue.prototype.$axios = axios


Vue.config.productionTip = false





// 全局守卫
router.beforeEach((to, from, next) => {
  // to 进入哪个路由
  // from从哪个路由离开
  // next进入
  if (store.getters.isLogin) {
    next()
  } else {
    if (to.path == '/Login' || to.path == '/Register') {
      next()
    } else {
      // alert('没登录')
      next('/Login')
    }
  }
})


// 后置钩子 进入路由后
// router.afterEach((to,from)=>{
//   // to
//   // from
//   alert('after Each')
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
