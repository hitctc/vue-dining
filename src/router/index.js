import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Menu from '../components/Menu'
import Admin from '../components/Admin'
import Login from '../components/Login'
import Register from '../components/Register'
import About from '../components/about/About'

// 二级路由
import contact from '../components/about/about_childer/contact'
import Delivery from '../components/about/about_childer/Delivery'
import History from '../components/about/about_childer/History'
import OrderingGuide from '../components/about/about_childer/OrderingGuide'
// 三级路由
import Phone from '../components/about/about_childer/contact_childer/Phone'
import Personname from '../components/about/about_childer/contact_childer/Personname'
import NewlyBuild from '../components/about/about_childer/contact_childer/NewlyBuild'
Vue.use(Router)

export default new Router({
  mode:'history',
  // scrollBehavior (to, from, savedPosition){
  // // 路由滚动
  //   // return {x:0,y:100}
  //   // return {
  //   //   selector:'.btn'
  //   // }

  //   // 如果点击浏览器后退，就返回上一次页面停留的地方
  //   // savedPosition 保存位置，保存上一个页面的位置，
    
  //   if (savedPosition){
  //     // 如果上一个页面有停留的地方
  //     return savedPosition
  //   }else{
  //     // 没有
  //     return {selector: '.btn'}
  //   }
  // },
  routes: [
    {
      path:'/', // 主页
      name:'HomeLink',
      components:{
        default:Home,
        'OrderingGuide': OrderingGuide,
        'Delivery': Delivery,
        'History': History

      }
    },
    {
      path: '/Menu', // 菜单
      name: 'MenuLink',
      component: Menu
    },
    {
      path: '/Admin', // 管理
      name: 'AdminLink',
      component: Admin,
      // beforeEnter: (to, from, next) => {
      //   // ...  路由独享守卫

      //   alert("非登陆，不能访问此页面")
      //   next('/Login')
      // }
    },
    {
      path: '/Login', // 登录
      name: 'LoginLink',
      component: Login
    },
    {
      path: '/Register', // 注册
      name: 'RegisterLink',
      component: Register
    },
    {
      path: '/About', // 关于我们
      name: 'AboutLink',
      component: About,
      redirect:'/About/contact',
      children:[
        {
          path:'/About/contact',
          name:'ContactLink',
          component:contact,
          redirect: '/About/contact/Phone',
          children:[
            {
              path:'/About/contact/Phone',
              name:'PhoneLink',
              component:Phone
            },
            {
              path: '/About/contact/Personname',
              name: 'PersonnameLink',
              component: Personname
            }
          ]
        },
        {
          path: '/About/Delivery',
          name: 'DeliveryLink',
          component: Delivery
        },
        {
          path: '/About/History',
          name: 'HistoryLink',
          component:History
        },
        {
          path: '/About/OrderingGuideLink',
          name: 'OrderingGuideLink',
          component: OrderingGuide
        },
        // {
        //   path: '/About/NewlyBuild',
        //   name: 'NewlyBuildLink',
        //   component: NewlyBuild
        // }
      ]
    },
    {
      path:'*',
      redirect:'/'
    }
  ]
})
